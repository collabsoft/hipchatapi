
package com.hipchat.api.v1;

import com.google.gson.annotations.SerializedName;
import com.hipchat.api.v1.HipChatApi.Method;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.TimeZone;

public class UserImpl implements User {
    
    @SerializedName("user_id")          private int id;
    @SerializedName("name")             private String name;
    @SerializedName("mention_name")     private String mentionName;
    @SerializedName("last_active")      private Date lastActive;
    @SerializedName("created")          private Date created;
    @SerializedName("email")            private String emailAddress;
    @SerializedName("title")            private String title;
    @SerializedName("photo_url")        private String photoUrl;
    @SerializedName("status")           private String status;
    @SerializedName("status_message")   private String statusMessage;
    @SerializedName("is_group_admin")   private boolean admin;
    @SerializedName("is_deleted")       private boolean deleted;
    @SerializedName("password")         private String password;
    private TimeZone timezone;
    
    protected UserImpl() {
        
    }
    
    protected UserImpl(int id) {
        
    }
    
    public UserImpl(String name, String emailAddress, String mentionName, String title, boolean admin, String password) {
        this.name = name;
        this.emailAddress = emailAddress;
        this.mentionName = mentionName;
        this.title = title;
        this.admin = admin;
        this.password = password;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getMentionName() {
        return mentionName;
    }

    @Override
    public void setMentionName(String mentionName) {
        this.mentionName = mentionName;
    }

    @Override
    public Date getLastActive() {
        return lastActive;
    }

    @Override
    public void setLastActive(Date lastActive) {
        this.lastActive = lastActive;
    }

    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public String getEmailAddress() {
        return emailAddress;
    }

    @Override
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getPhotoUrl() {
        return photoUrl;
    }

    @Override
    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String getStatusMessage() {
        return statusMessage;
    }

    @Override
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    @Override
    public boolean isAdmin() {
        return admin;
    }

    @Override
    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    @Override
    public boolean isDeleted() {
        return deleted;
    }

    @Override
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public TimeZone getTimezone() {
        return timezone;
    }

    @Override
    public void setTimezone(TimeZone timezone) {
        this.timezone = timezone;
    }

    @Override
    public boolean create() {
        User newUser = getInstance().getObject("users/create", toString(Action.Create), Method.POST, UserImpl.class, "user");
        if(newUser != null) {
            getInstance().flushCache();
            this.password = newUser.getPassword();
            return true;
        }
        return false;
    }

    @Override
    public boolean update() {
        return getInstance().execute("users/update", toString(Action.Update), Method.POST);
    }

    @Override
    public boolean delete() {
        return getInstance().execute("users/delete", toString(Action.Delete), Method.POST);
    }

    @Override
    public boolean undelete() {
        return getInstance().execute("users/undelete", toString(Action.Undelete), Method.POST);
    }

    @Override
    public String toString(Action action) {
        
        switch(action) {
                            
            case Delete:
            case Undelete:
            case Show:
                return getQueryStringWithOnlyUserId();
                
            case Update:
                return getQueryStringForUpdate();
                
            case Create:
            default:
                return getQueryStringForCreation();

        }
        
    }
    
    // ----------------------------------------------------------------------------------------------- Private Methods
    
    private static HipChatApiImpl getInstance() {
        return HipChatApiImpl.INSTANCE;
    }

    private String getQueryStringForCreation() throws IllegalArgumentException {
        // Throwing an unchecked runtime exception on purpose:
        // http://tutorials.jenkov.com/java-exception-handling/checked-or-unchecked-exceptions.html
        IllegalArgumentException exception = null; 
        if (name == null || name.isEmpty()) {
            exception =  new IllegalArgumentException(Error.REQUIRED_PARAMETER_NAME);
        } else if (emailAddress == null || emailAddress.isEmpty()) {
            exception =  new IllegalArgumentException(Error.REQUIRED_PARAMETER_EMAILADDRESS);
        }
        
        StringBuilder qs = new StringBuilder();
        try {
            qs.append(String.format("email=%s", URLEncoder.encode(emailAddress, "UTF-8")));
            qs.append(String.format("&name=%s", URLEncoder.encode(name, "UTF-8")));
            qs.append(String.format("&is_group_admin=%s", (admin ? "1" : "0")));
            if(mentionName != null) {
                qs.append(String.format("&mention_name=%s", URLEncoder.encode(mentionName, "UTF-8")));
            }
            if(title != null) {
                qs.append(String.format("&title=%s", URLEncoder.encode(title, "UTF-8")));
            }
            if(password != null) {
                qs.append(String.format("&password=%s", URLEncoder.encode(password, "UTF-8")));
            }
            if(timezone != null) {
                qs.append(String.format("&timezone=%s", URLEncoder.encode(timezone.getID(), "UTF-8")));
            }
        } catch(UnsupportedEncodingException e) {
            exception = new IllegalArgumentException(Error.GENERIC_ERROR_MESSAGE, e);
        }

        if(exception != null) {
            getInstance().setLastErrorException(exception);
            throw exception;
        }
        
        return qs.toString();
    }

    private String getQueryStringForUpdate() throws IllegalArgumentException {
        // Throwing an unchecked runtime exception on purpose:
        // http://tutorials.jenkov.com/java-exception-handling/checked-or-unchecked-exceptions.html
        IllegalArgumentException exception = null; 
        if (name == null || name.isEmpty()) {
            exception = new IllegalArgumentException(Error.REQUIRED_PARAMETER_NAME);
        } else if (emailAddress == null || emailAddress.isEmpty()) {
            exception = new IllegalArgumentException(Error.REQUIRED_PARAMETER_EMAILADDRESS);
        }
        
        StringBuilder qs = new StringBuilder();
        try {
            qs.append(getQueryStringWithOnlyUserId());
            qs.append(String.format("&email=%s", URLEncoder.encode(emailAddress, "UTF-8")));
            qs.append(String.format("&name=%s", URLEncoder.encode(name, "UTF-8")));
            qs.append(String.format("&is_group_admin=%s", (admin ? "1" : "0")));

            if(mentionName != null) {
                qs.append(String.format("&mention_name=%s", URLEncoder.encode(mentionName, "UTF-8")));
            }
            if(title != null) {
                qs.append(String.format("&title=%s", URLEncoder.encode(title, "UTF-8")));
            }
            if(password != null) {
                qs.append(String.format("&password=%s", URLEncoder.encode(password, "UTF-8")));
            }
            if(timezone != null) {
                qs.append(String.format("&timezone=%s", URLEncoder.encode(timezone.getID(), "UTF-8")));
            }
        } catch(UnsupportedEncodingException e) {
            exception = new IllegalArgumentException(Error.GENERIC_ERROR_MESSAGE, e);
        }
        
        if(exception != null) {
            getInstance().setLastErrorException(exception);
            throw exception;
        }
        
        return qs.toString();
    }

    private String getQueryStringWithOnlyUserId() throws IllegalArgumentException {
        if (id <= 0) {
            getInstance().setLastErrorException(new IllegalArgumentException(Error.REQUIRED_PARAMETER_ID));
            throw (IllegalArgumentException)getInstance().getLastErrorException();
        }
        
        StringBuilder qs = new StringBuilder();
        qs.append(String.format("user_id=%s", id));

        return qs.toString();
    }
    
    

    
    
}
