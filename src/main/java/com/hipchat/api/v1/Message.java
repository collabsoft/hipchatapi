
package com.hipchat.api.v1;

import java.util.Date;

public interface Message {

    public Date getDate();
    public void setDate(Date date);
    public UserImpl getAuthor();
    public void setAuthor(UserImpl author);
    public String getMessage();
    public void setMessage(String message);
    
}
