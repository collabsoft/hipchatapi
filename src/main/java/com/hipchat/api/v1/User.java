
package com.hipchat.api.v1;

import java.util.Date;
import java.util.TimeZone;

public interface User {

    public static enum Action { Create, Update, Delete, Undelete, Show };
    
    public int getId();
    public void setId(int id);
    public String getName();
    public void setName(String name);
    public String getMentionName();
    public void setMentionName(String mentionName);
    public Date getLastActive();
    public void setLastActive(Date lastActive);
    public Date getCreated();
    public void setCreated(Date created);
    public String getEmailAddress();
    public void setEmailAddress(String emailAddress);
    public String getTitle();
    public void setTitle(String title);
    public String getPhotoUrl();
    public void setPhotoUrl(String photoUrl);
    public String getStatus();
    public void setStatus(String status);
    public String getStatusMessage();
    public void setStatusMessage(String statusMessage);
    public boolean isAdmin();
    public void setAdmin(boolean admin);
    public boolean isDeleted();
    public void setDeleted(boolean deleted);
    public String getPassword();
    public void setPassword(String password);
    public TimeZone getTimezone();
    public void setTimezone(TimeZone timezone);
    
    public boolean create();
    public boolean update();
    public boolean delete();
    public boolean undelete();
    
    public String toString(Action action);
    
}
